#!/usr/bin/env python3
from flask import Flask, jsonify, make_response
from flask_restful import reqparse, Resource, Api
from flask_session import Session
from flask_cors import CORS

import settings  # Our server and db settings, stored in settings.py
from resources.items import Items, Item
from resources.users import Users, User
from resources.wishes import Wish, Wishes
from resources.signin import SignIn

app = Flask(__name__)
CORS(app)
app.secret_key = settings.SECRET_KEY
app.config['SESSION_TYPE'] = 'filesystem'
app.config['SESSION_COOKIE_NAME'] = 'peanutButter'
app.config['SESSION_COOKIE_DOMAIN'] = settings.APP_HOST
Session(app)


@app.errorhandler(400)  # decorators to add to 400 response
def not_found(error):
    return make_response(jsonify({'status': 'Bad request'}), 400)


@app.errorhandler(404)  # decorators to add to 404 response
def not_found(error):
    return make_response(jsonify({'status': 'Resource not found'}), 404)


api = Api(app)
api.add_resource(SignIn, '/signin')
api.add_resource(Items, '/items')
api.add_resource(Item, '/items/<int:pk>')
api.add_resource(Users, '/users')
api.add_resource(User, '/users/<int:pk>')
api.add_resource(Wishes, '/users/<int:user_id>/wishes')
api.add_resource(Wish, '/users/<int:user_id>/wishes/<int:wish_id>')

if __name__ == "__main__":
    context = ('cert.pem', 'key.pem')  # Identify the certificates you've generated.
    app.run(
        host=settings.APP_HOST,
        port=settings.APP_PORT,
        ssl_context=context,
        debug=settings.APP_DEBUG)
