import pymysql

import settings

connection = pymysql.connect(
    settings.DB_HOST,
    settings.DB_USER,
    settings.DB_PASSWD,
    settings.DB_DATABASE,
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)


def call_procedure(procedure, *args):
    c = connection.cursor()
    c.callproc(procedure, args)
    rows = c.fetchall()
    connection.commit()
    return rows


def get_item(pk):
    return call_procedure("get_item", pk)


def get_items():
    rows = call_procedure("get_items")
    return rows


def create_item(name, description):
    call_procedure("create_item", name, description)


def delete_item(pk):
    call_procedure("delete_item", pk)


def update_item(pk, name, description):
    call_procedure("update_item", pk, name, description)


def get_user(pk):
    return call_procedure("get_user", pk)


def get_users():
    rows = call_procedure("get_users")
    return rows


def create_user(username):
    
    call_procedure("create_user", username)


def delete_user(pk):
    call_procedure("delete_user", pk)


def update_user(pk, username):
    call_procedure("update_user", pk, username)


def get_wishes(user_id):
    rows = call_procedure("get_wishes", user_id)
    return rows


def create_wish(item_id, user_id):
    call_procedure("create_wish", item_id, user_id)


def delete_wish(pk):
    call_procedure("delete_wish", pk)

