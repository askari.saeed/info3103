from flask import request, abort, session, make_response, jsonify
from flask_restful import reqparse, Resource
from ldap3 import Server, Connection, ALL
from ldap3.core.exceptions import *

import settings
from db import get_users, create_user
from auth import login_required

def get_or_create_user(username):
    for user in get_users():
        if user['username'] == username:
            return user
    create_user(username)
    for user in get_users():
        if user['username'] == username:
            return user

class SignIn(Resource):

    @staticmethod
    def post():
        if not request.json:
            abort(400)  # bad request

        # Parse the json
        parser = reqparse.RequestParser()
        try:
            # Check for required attributes in json document, create a dictionary
            parser.add_argument('username', type=str, required=True)
            parser.add_argument('password', type=str, required=True)
            request_params = parser.parse_args()
        except Exception:
            abort(400)  # bad request

        if 'username' in session:
            user = get_or_create_user(session['username'])
            response = user
            responseCode = 200
        else:
            try:
                ldapServer = Server(host=settings.LDAP_HOST)
                ldapConnection = Connection(ldapServer,
                                            raise_exceptions=True,
                                            user='uid='
                                                 + request_params['username'] +
                                                 ', ou=People,ou=fcs,o=unb',
                                            password=request_params['password'])
                ldapConnection.open()
                ldapConnection.start_tls()
                ldapConnection.bind()
                # At this point we have sucessfully authenticated.
                session['username'] = request_params['username']
                response = get_or_create_user(session['username'])
                responseCode = 201
            except LDAPException:
                response = {'status': 'Access denied'}
                responseCode = 403
            finally:
                ldapConnection.unbind()

        return make_response(jsonify(response), responseCode)

    #@login_required
    #def get(self):
    #    return make_response(jsonify({'status': 'already logged in',
    #                                  'username': session['username']}), 200)

    def delete(self):
        if 'username' in session:
            session.pop('username')
        return make_response(jsonify({}), 204)
