from flask import jsonify, make_response, request
from flask_restful import Resource, reqparse

from auth import check_authorized
from db import get_wishes, create_wish, delete_wish

parser = reqparse.RequestParser()
parser.add_argument('item_id', type=str, required=True)

class Wishes(Resource):
    @staticmethod
    def get(user_id):
        return make_response(jsonify(get_wishes(user_id)), 200)

    @staticmethod
    def post(user_id):
        check_authorized(user_id)
        args = parser.parse_args()
        create_wish(args['item_id'], user_id)
        return make_response(jsonify({}), 201)


class Wish(Resource):

    @staticmethod
    def delete(user_id, wish_id):
        check_authorized(user_id)
        delete_wish(wish_id)
        return make_response(jsonify({}), 204)

