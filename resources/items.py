from flask import jsonify, make_response, request, abort
from flask_restful import Resource, reqparse

from db import get_items, create_item, get_item, delete_item, update_item


parser = reqparse.RequestParser()
parser.add_argument('name', type=str, required=True)
parser.add_argument('description', type=str, required=True)

class Items(Resource):
    @staticmethod
    def get():
        return make_response(jsonify(get_items()), 200)

    @staticmethod
    def post():
        args = parser.parse_args() 
        create_item(args['name'], args['description'])
        return make_response(jsonify({}), 201)


class Item(Resource):
    @staticmethod
    def get(pk):
        rows = get_item(pk)
        if not rows:
            abort(404, "Not Found")
        return make_response(jsonify(rows[0]), 200)

    @staticmethod
    def delete(pk):
        delete_item(pk)
        return make_response(jsonify({}), 204)

    @staticmethod
    def put(pk):
        args = parser.parse_args()
        update_item(pk, args['name'], args['description'])
        return make_response(jsonify({}), 204)
