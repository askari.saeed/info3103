import pymysql.cursors

import settings

CREATE_TABLE_ITEMS = """
CREATE TABLE items (
    id int not null auto_increment,
    name char(100) not null,
    description char(255) not null,
    primary key(id)
    );
"""

CREATE_TABLE_USERS = """
CREATE TABLE users (
    id int not null auto_increment,
    username char(30) not null unique,
    primary key(id)
    );
"""

CREATE_TABLE_WISHES = """
CREATE TABLE wishes (
    id INT NOT NULL auto_increment,
    item_id INT NOT NULL,
    user_id INT NOT NULL,
    primary key(id),
    FOREIGN KEY (item_id) REFERENCES items(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
    );
"""

INSERT_WISH = """
DROP PROCEDURE IF EXISTS create_wish;
CREATE PROCEDURE create_wish(
    IN _item_id INT,
    IN _user_id INT
)
    INSERT INTO wishes (item_id, user_id) VALUES (_item_id, _user_id);
"""

GET_WISHES = """
DROP PROCEDURE IF EXISTS get_wishes;
CREATE PROCEDURE get_wishes(
    IN _user_id INT
)
    SELECT * FROM wishes WHERE user_id=_user_id;
"""

GET_WISH = """
DROP PROCEDURE IF EXISTS get_wish;
CREATE PROCEDURE get_wish(
        IN _id INT
    )
    SELECT * FROM wishes WHERE id=_id;
"""

DELETE_WISH = """
DROP PROCEDURE IF EXISTS delete_wish;
CREATE PROCEDURE delete_wish(
        IN _id INT
    )
    DELETE FROM wishes WHERE id=_id;
"""

INSERT_USER = """
DROP PROCEDURE IF EXISTS create_user;
CREATE PROCEDURE create_user(
        IN _username CHAR(30)
    )
    INSERT INTO users (username) VALUES (_username);
"""

GET_USERS = """
DROP PROCEDURE IF EXISTS get_users;
CREATE PROCEDURE get_users()
    SELECT * FROM users;
"""

GET_USER = """
DROP PROCEDURE IF EXISTS get_user;
CREATE PROCEDURE get_user(
        IN _id INT
    )
    SELECT * FROM users WHERE id=_id;
"""

DELETE_USER = """
DROP PROCEDURE IF EXISTS delete_user;
CREATE PROCEDURE delete_user(
        IN _id INT
    )
    DELETE FROM users WHERE id=_id;
"""

UPDATE_USER = """
DROP PROCEDURE IF EXISTS update_user;
CREATE PROCEDURE update_user(
        IN _id INT,
        IN _username CHAR(30)
    )
    UPDATE users SET username=_username WHERE id=_id;
"""

INSERT_ITEM = """
DROP PROCEDURE IF EXISTS create_item;
CREATE PROCEDURE create_item(
        IN name CHAR(100), 
        IN description CHAR(255)
    )
    INSERT INTO items (name, description) VALUES (name, description);
"""

GET_ITEMS = """
DROP PROCEDURE IF EXISTS get_items;
CREATE PROCEDURE get_items()
    SELECT * FROM items;
"""

GET_ITEM = """
DROP PROCEDURE IF EXISTS get_item;
CREATE PROCEDURE get_item(
        IN _id INT
    )
    SELECT * FROM items WHERE id=_id;
"""

DELETE_ITEM = """
DROP PROCEDURE IF EXISTS delete_item;
CREATE PROCEDURE delete_item(
        IN _id INT
    )
    DELETE FROM items WHERE id=_id;
"""

UPDATE_ITEM = """
DROP PROCEDURE IF EXISTS update_item;
CREATE PROCEDURE update_item(
        IN _id INT,
        IN _name CHAR(100),
        IN _description CHAR(100)
    )
    UPDATE items SET name=_name, description=_description WHERE id=_id;
"""

DROP_TABLES = """
DROP TABLE IF EXISTS wishes;
DROP TABLE IF EXISTS items;
DROP TABLE IF EXISTS users;
"""

commands = [
    DROP_TABLES,

    CREATE_TABLE_ITEMS,
    CREATE_TABLE_USERS,
    CREATE_TABLE_WISHES,

    INSERT_ITEM,
    GET_ITEMS,
    GET_ITEM,
    DELETE_ITEM,
    UPDATE_ITEM,

    INSERT_USER,
    GET_USERS,
    GET_USER,
    DELETE_USER,
    UPDATE_USER,

    INSERT_WISH,
    GET_WISHES,
    DELETE_WISH
]

dbConnection = pymysql.connect(
    settings.DB_HOST,
    settings.DB_USER,
    settings.DB_PASSWD,
    settings.DB_DATABASE,
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)
cursor = dbConnection.cursor()
for command in commands:
    cursor.execute(command)
dbConnection.commit()
